#!/bin/bash

USER=ec2-user
AWSCONFIG=/home/$USER/.aws

. "/etc/parallelcluster/cfnconfig"
case "${cfn_node_type}" in
    MasterServer)
        echo "MasterServer Setup"
        
        # aws credentials
        mkdir $AWSCONFIG
        cat <<EOM > $AWSCONFIG/config
[default]
output = json
region = cn-northwest-1
EOM

        cat <<EOM > $AWSCONFIG/credentials
[default]
aws_access_key_id = $2
aws_secret_access_key = $3
EOM

        chown -R $USER:$USER $AWSCONFIG
        # sync home directory
        runuser -l $USER -c "aws s3 sync $4 /home/$USER"
        
        # install packages
        yum -y install "${@:5}"
    ;;
    ComputeFleet)
        echo "ComputeFleet Setup"
        # add additional local scratch volume
    ;;
    *)
        ;;
esac
